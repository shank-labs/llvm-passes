#include "stdio.h"

int bar(char c);

typedef struct {
    char c;
    char d;
} S;

void foo(int x) {}

int bar(char c) { return 1; }

int main(int argc, char **argv) { return 0; }

void baz() {
    /* for (int i = 0; i < 10; i++) { */
    /*     printf("%d\n", i); */
    /* } */

    S sinst[2];
    sinst[1].d = 'a';
    printf("%c\n", sinst[1].d);

    int a[5];
    a[1] = 2;
}
