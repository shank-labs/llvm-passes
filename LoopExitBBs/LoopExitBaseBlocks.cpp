#include <llvm/Analysis/LoopInfo.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/Pass.h>
#include <llvm/Support/Debug.h>

using namespace llvm;

namespace {
    struct LoopExitBBPass : public ModulePass {
      public:
        static char ID;

        LoopExitBBPass() : ModulePass(ID) {}

        ~LoopExitBBPass(){};

        bool runOnModule(Module &M) override {
            for (auto &currFunc : M) {
                if (!currFunc.isDeclaration()) {
                    LoopInfoWrapperPass &loopAnalysis = getAnalysis<LoopInfoWrapperPass>(currFunc);
                    LoopInfo &loopInfo = loopAnalysis.getLoopInfo();

                    for (Loop *loop : loopInfo.getLoopsInPreorder()) {
                        SmallVector<BasicBlock *, 32> exitBBs;
                        exitBBs.clear();
                        loop->getExitingBlocks(exitBBs);

                        for (BasicBlock *bb : exitBBs) {
                            Instruction *exitInstr = bb->getTerminator();
                            dbgs() << *exitInstr << '\n';
                            const DebugLoc &loc = exitInstr->getDebugLoc();
                            if (loc) {
                                DILocation *diLoc = loc.get();
                                if (diLoc) {
                                    dbgs() << diLoc->getLine() << '\n';
                                } else {
                                    dbgs() << "diLoc is nullptr\n";
                                }
                                /* dbgs() << " at " << diLoc->getLine() << " " << M.getName(); */

                            } else {
                                dbgs() << "loc is nullptr\n";
                            }
                        }
                    }
                }
            }

            return false;
        }

        void getAnalysisUsage(AnalysisUsage &AU) const override {
            AU.addRequired<LoopInfoWrapperPass>();
        }
    };

    char LoopExitBBPass::ID = 0;

    static RegisterPass<LoopExitBBPass> X("loopExitBBs", "Exit instructions for loops", false,
                                          false);
} // namespace
