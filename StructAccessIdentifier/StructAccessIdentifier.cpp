#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/Pass.h>
#include <llvm/Support/Casting.h>
#include <llvm/Support/Debug.h>

using namespace llvm;

namespace {
    struct StructAccessIdentifierPass : public FunctionPass {
        static char ID;

        StructAccessIdentifierPass() : FunctionPass(ID) {}
        ~StructAccessIdentifierPass() {}

        bool runOnFunction(Function &F) {
            dbgs() << "[+] " << F.getName() << '\n';
            dbgs() << "F.isDeclaration: " << (F.isDeclaration() ? "true" : "false") << '\n';

            if (!F.isDeclaration()) {
                dbgs() << F;
            }

            for (BasicBlock &bb : F) {
                for (Instruction &inst : bb) {
                    if (GetElementPtrInst *gepInst = dyn_cast<GetElementPtrInst>(&inst)) {
                        dbgs() << "found GEP\n";
                        dbgs() << "gepInst >>> " << *gepInst << '\n';
                        dbgs() << "pointerOperandType >>> " << *gepInst->getPointerOperandType()
                               << '\n';

                        Type *currType = gepInst->getPointerOperandType();

                        while (currType != nullptr && dyn_cast<PointerType>(currType) != nullptr) {
                            currType = currType->getPointerElementType();
                            dbgs() << "currType >>> " << *currType << '\n';
                        }

                        // when control reaches here, currType no longer is a pointer type
                        if (currType->isStructTy()) {
                            StructType *structType = dyn_cast<StructType>(currType);
                            dbgs() << "structType >>> " << *structType << " : "
                                   << structType->getName().str() << '\n';
                        }
                    };
                }
            }

            return false;
        }
    };

    char StructAccessIdentifierPass::ID = 0;

    static RegisterPass<StructAccessIdentifierPass>
        X("structAccessIdent", "Pass to identify all accesses to structs", false, false);

} // namespace
