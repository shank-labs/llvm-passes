; ModuleID = 'simple_functions_inst.bc'
source_filename = "simple_functions.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.S = type { i8, i8 }

@.str = private unnamed_addr constant [4 x i8] c"%c\0A\00", align 1

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @foo(i32 %0) #0 !dbg !11 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !15, metadata !DIExpression()), !dbg !16
  ret void, !dbg !17
}

; Function Attrs: nofree nosync nounwind readnone speculatable willreturn
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @bar(i8 signext %0) #0 !dbg !18 {
  %2 = alloca i8, align 1
  store i8 %0, i8* %2, align 1
  call void @llvm.dbg.declare(metadata i8* %2, metadata !22, metadata !DIExpression()), !dbg !23
  ret i32 1, !dbg !24
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @main(i32 %0, i8** %1) #0 !dbg !25 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  call void @llvm.dbg.declare(metadata i32* %4, metadata !30, metadata !DIExpression()), !dbg !31
  store i8** %1, i8*** %5, align 8
  call void @llvm.dbg.declare(metadata i8*** %5, metadata !32, metadata !DIExpression()), !dbg !33
  ret i32 0, !dbg !34
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @baz() #0 !dbg !35 {
  %1 = alloca [2 x %struct.S], align 1
  %2 = alloca [5 x i32], align 16
  call void @llvm.dbg.declare(metadata [2 x %struct.S]* %1, metadata !38, metadata !DIExpression()), !dbg !47
  %3 = getelementptr inbounds [2 x %struct.S], [2 x %struct.S]* %1, i64 0, i64 1, !dbg !48
  %4 = getelementptr inbounds %struct.S, %struct.S* %3, i32 0, i32 1, !dbg !49
  store i8 97, i8* %4, align 1, !dbg !50
  %5 = getelementptr inbounds [2 x %struct.S], [2 x %struct.S]* %1, i64 0, i64 1, !dbg !51
  %6 = getelementptr inbounds %struct.S, %struct.S* %5, i32 0, i32 1, !dbg !52
  %7 = load i8, i8* %6, align 1, !dbg !52
  %8 = sext i8 %7 to i32, !dbg !51
  %9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i64 0, i64 0), i32 %8), !dbg !53
  call void @llvm.dbg.declare(metadata [5 x i32]* %2, metadata !54, metadata !DIExpression()), !dbg !58
  %10 = getelementptr inbounds [5 x i32], [5 x i32]* %2, i64 0, i64 1, !dbg !59
  store i32 2, i32* %10, align 4, !dbg !60
  ret void, !dbg !61
}

declare i32 @printf(i8*, ...) #2

attributes #0 = { noinline nounwind optnone sspstrong uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { nofree nosync nounwind readnone speculatable willreturn }
attributes #2 = { "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!3, !4, !5, !6, !7, !8, !9}
!llvm.ident = !{!10}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 13.0.0", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2, splitDebugInlining: false, nameTableKind: None)
!1 = !DIFile(filename: "simple_functions.c", directory: "/home/shank/code/research/llvm-passes-lab")
!2 = !{}
!3 = !{i32 7, !"Dwarf Version", i32 4}
!4 = !{i32 2, !"Debug Info Version", i32 3}
!5 = !{i32 1, !"wchar_size", i32 4}
!6 = !{i32 7, !"PIC Level", i32 2}
!7 = !{i32 7, !"PIE Level", i32 2}
!8 = !{i32 7, !"uwtable", i32 1}
!9 = !{i32 7, !"frame-pointer", i32 2}
!10 = !{!"clang version 13.0.0"}
!11 = distinct !DISubprogram(name: "foo", scope: !1, file: !1, line: 10, type: !12, scopeLine: 10, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !2)
!12 = !DISubroutineType(types: !13)
!13 = !{null, !14}
!14 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!15 = !DILocalVariable(name: "x", arg: 1, scope: !11, file: !1, line: 10, type: !14)
!16 = !DILocation(line: 10, column: 14, scope: !11)
!17 = !DILocation(line: 10, column: 18, scope: !11)
!18 = distinct !DISubprogram(name: "bar", scope: !1, file: !1, line: 12, type: !19, scopeLine: 12, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !2)
!19 = !DISubroutineType(types: !20)
!20 = !{!14, !21}
!21 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!22 = !DILocalVariable(name: "c", arg: 1, scope: !18, file: !1, line: 12, type: !21)
!23 = !DILocation(line: 12, column: 14, scope: !18)
!24 = !DILocation(line: 12, column: 19, scope: !18)
!25 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 14, type: !26, scopeLine: 14, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !2)
!26 = !DISubroutineType(types: !27)
!27 = !{!14, !14, !28}
!28 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !29, size: 64)
!29 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !21, size: 64)
!30 = !DILocalVariable(name: "argc", arg: 1, scope: !25, file: !1, line: 14, type: !14)
!31 = !DILocation(line: 14, column: 14, scope: !25)
!32 = !DILocalVariable(name: "argv", arg: 2, scope: !25, file: !1, line: 14, type: !28)
!33 = !DILocation(line: 14, column: 27, scope: !25)
!34 = !DILocation(line: 14, column: 35, scope: !25)
!35 = distinct !DISubprogram(name: "baz", scope: !1, file: !1, line: 16, type: !36, scopeLine: 16, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !2)
!36 = !DISubroutineType(types: !37)
!37 = !{null}
!38 = !DILocalVariable(name: "sinst", scope: !35, file: !1, line: 21, type: !39)
!39 = !DICompositeType(tag: DW_TAG_array_type, baseType: !40, size: 32, elements: !45)
!40 = !DIDerivedType(tag: DW_TAG_typedef, name: "S", file: !1, line: 8, baseType: !41)
!41 = distinct !DICompositeType(tag: DW_TAG_structure_type, file: !1, line: 5, size: 16, elements: !42)
!42 = !{!43, !44}
!43 = !DIDerivedType(tag: DW_TAG_member, name: "c", scope: !41, file: !1, line: 6, baseType: !21, size: 8)
!44 = !DIDerivedType(tag: DW_TAG_member, name: "d", scope: !41, file: !1, line: 7, baseType: !21, size: 8, offset: 8)
!45 = !{!46}
!46 = !DISubrange(count: 2)
!47 = !DILocation(line: 21, column: 7, scope: !35)
!48 = !DILocation(line: 22, column: 5, scope: !35)
!49 = !DILocation(line: 22, column: 14, scope: !35)
!50 = !DILocation(line: 22, column: 16, scope: !35)
!51 = !DILocation(line: 23, column: 20, scope: !35)
!52 = !DILocation(line: 23, column: 29, scope: !35)
!53 = !DILocation(line: 23, column: 5, scope: !35)
!54 = !DILocalVariable(name: "a", scope: !35, file: !1, line: 25, type: !55)
!55 = !DICompositeType(tag: DW_TAG_array_type, baseType: !14, size: 160, elements: !56)
!56 = !{!57}
!57 = !DISubrange(count: 5)
!58 = !DILocation(line: 25, column: 9, scope: !35)
!59 = !DILocation(line: 26, column: 5, scope: !35)
!60 = !DILocation(line: 26, column: 10, scope: !35)
!61 = !DILocation(line: 27, column: 1, scope: !35)
