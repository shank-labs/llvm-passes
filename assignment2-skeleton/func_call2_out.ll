; ModuleID = 'func_call2_out.bc'
source_filename = "func_call2.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@arr = dso_local global [100 x i32] zeroinitializer, align 16, !dbg !0
@l = dso_local global i32 0, align 4, !dbg !6
@.str = private unnamed_addr constant [28 x i8] c"This should not be skipped\0A\00", align 1
@.str.1 = private unnamed_addr constant [28 x i8] c"This should be skipped too\0A\00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c"%d\00", align 1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @bar(i32 %0) #0 !dbg !16 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !19, metadata !DIExpression()), !dbg !20
  call void @llvm.dbg.declare(metadata i32* %3, metadata !21, metadata !DIExpression()), !dbg !22
  store i32 0, i32* %3, align 4, !dbg !23
  br label %4, !dbg !25

4:                                                ; preds = %12, %1
  %5 = load i32, i32* %3, align 4, !dbg !26
  %6 = load i32, i32* %2, align 4, !dbg !28
  %7 = icmp slt i32 %5, %6, !dbg !29
  br i1 %7, label %8, label %15, !dbg !30

8:                                                ; preds = %4
  %9 = load i32, i32* %3, align 4, !dbg !31
  %10 = sext i32 %9 to i64, !dbg !33
  %11 = getelementptr inbounds [100 x i32], [100 x i32]* @arr, i64 0, i64 %10, !dbg !33
  store i32 0, i32* %11, align 4, !dbg !34
  br label %12, !dbg !35

12:                                               ; preds = %8
  %13 = load i32, i32* %3, align 4, !dbg !36
  %14 = add nsw i32 %13, 1, !dbg !36
  store i32 %14, i32* %3, align 4, !dbg !36
  br label %4, !dbg !37, !llvm.loop !38

15:                                               ; preds = %4
  %16 = load i32, i32* @l, align 4, !dbg !41
  %17 = add nsw i32 %16, 1, !dbg !41
  store i32 %17, i32* @l, align 4, !dbg !41
  call void @baz(), !dbg !42
  %18 = load i32, i32* %2, align 4, !dbg !43
  %19 = sub nsw i32 %18, 1, !dbg !44
  %20 = sext i32 %19 to i64, !dbg !45
  %21 = getelementptr inbounds [100 x i32], [100 x i32]* @arr, i64 0, i64 %20, !dbg !45
  %22 = load i32, i32* %21, align 4, !dbg !45
  ret i32 %22, !dbg !46
}

; Function Attrs: nofree nosync nounwind readnone speculatable willreturn
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @foo(i32 %0) #0 !dbg !47 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !50, metadata !DIExpression()), !dbg !51
  %3 = load i32, i32* %2, align 4, !dbg !52
  %4 = icmp slt i32 %3, 0, !dbg !54
  br i1 %4, label %5, label %6, !dbg !55

5:                                                ; preds = %1
  call void @baz(), !dbg !56
  br label %9, !dbg !58

6:                                                ; preds = %1
  %7 = load i32, i32* %2, align 4, !dbg !59
  %8 = call i32 @bar(i32 %7), !dbg !61
  br label %9

9:                                                ; preds = %6, %5
  call void @exit(i32 -1), !dbg !62
  ret void, !dbg !62
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @baz() #0 !dbg !63 {
  %1 = load i32, i32* @l, align 4, !dbg !66
  %2 = icmp slt i32 %1, 0, !dbg !68
  br i1 %2, label %3, label %5, !dbg !69

3:                                                ; preds = %0
  %4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i64 0, i64 0)), !dbg !70
  br label %8, !dbg !72

5:                                                ; preds = %0
  %6 = load i32, i32* @l, align 4, !dbg !73
  %7 = call i32 @bar(i32 %6), !dbg !75
  br label %8

8:                                                ; preds = %5, %3
  ret void, !dbg !76
}

declare dso_local i32 @printf(i8*, ...) #2

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main(i32 %0, i8** %1) #0 !dbg !77 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca i32, align 4
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  call void @llvm.dbg.declare(metadata i32* %4, metadata !83, metadata !DIExpression()), !dbg !84
  store i8** %1, i8*** %5, align 8
  call void @llvm.dbg.declare(metadata i8*** %5, metadata !85, metadata !DIExpression()), !dbg !86
  call void @llvm.dbg.declare(metadata i32* %6, metadata !87, metadata !DIExpression()), !dbg !88
  %7 = load i32, i32* %4, align 4, !dbg !89
  %8 = icmp sgt i32 %7, 2, !dbg !91
  br i1 %8, label %9, label %12, !dbg !92

9:                                                ; preds = %2
  %10 = load i32, i32* %4, align 4, !dbg !93
  %11 = call i32 @bar(i32 %10), !dbg !95
  br label %14, !dbg !96

12:                                               ; preds = %2
  call void @exit(i32 -1), !dbg !97
  %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i64 0, i64 0)), !dbg !97
  store i32 -1, i32* %3, align 4, !dbg !99
  br label %17, !dbg !99

14:                                               ; preds = %9
  %15 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i64 0, i64 0), i32* %6), !dbg !100
  %16 = load i32, i32* %6, align 4, !dbg !101
  call void @foo(i32 %16), !dbg !102
  store i32 0, i32* %3, align 4, !dbg !103
  br label %17, !dbg !103

17:                                               ; preds = %14, %12
  call void @exit(i32 -1), !dbg !104
  %18 = load i32, i32* %3, align 4, !dbg !104
  ret i32 %18, !dbg !104
}

declare dso_local i32 @__isoc99_scanf(i8*, ...) #2

declare void @exit(i32)

attributes #0 = { noinline nounwind optnone uwtable "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nofree nosync nounwind readnone speculatable willreturn }
attributes #2 = { "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!12, !13, !14}
!llvm.ident = !{!15}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "arr", scope: !2, file: !3, line: 3, type: !9, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 12.0.1", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5, splitDebugInlining: false, nameTableKind: None)
!3 = !DIFile(filename: "func_call2.c", directory: "/home/shank/code/research/llvm-passes-lab/assignment2-skeleton/testcases/function_prioritization")
!4 = !{}
!5 = !{!0, !6}
!6 = !DIGlobalVariableExpression(var: !7, expr: !DIExpression())
!7 = distinct !DIGlobalVariable(name: "l", scope: !2, file: !3, line: 4, type: !8, isLocal: false, isDefinition: true)
!8 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!9 = !DICompositeType(tag: DW_TAG_array_type, baseType: !8, size: 3200, elements: !10)
!10 = !{!11}
!11 = !DISubrange(count: 100)
!12 = !{i32 7, !"Dwarf Version", i32 4}
!13 = !{i32 2, !"Debug Info Version", i32 3}
!14 = !{i32 1, !"wchar_size", i32 4}
!15 = !{!"clang version 12.0.1"}
!16 = distinct !DISubprogram(name: "bar", scope: !3, file: !3, line: 6, type: !17, scopeLine: 6, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!17 = !DISubroutineType(types: !18)
!18 = !{!8, !8}
!19 = !DILocalVariable(name: "i", arg: 1, scope: !16, file: !3, line: 6, type: !8)
!20 = !DILocation(line: 6, column: 13, scope: !16)
!21 = !DILocalVariable(name: "j", scope: !16, file: !3, line: 7, type: !8)
!22 = !DILocation(line: 7, column: 9, scope: !16)
!23 = !DILocation(line: 8, column: 12, scope: !24)
!24 = distinct !DILexicalBlock(scope: !16, file: !3, line: 8, column: 5)
!25 = !DILocation(line: 8, column: 10, scope: !24)
!26 = !DILocation(line: 8, column: 17, scope: !27)
!27 = distinct !DILexicalBlock(scope: !24, file: !3, line: 8, column: 5)
!28 = !DILocation(line: 8, column: 21, scope: !27)
!29 = !DILocation(line: 8, column: 19, scope: !27)
!30 = !DILocation(line: 8, column: 5, scope: !24)
!31 = !DILocation(line: 9, column: 13, scope: !32)
!32 = distinct !DILexicalBlock(scope: !27, file: !3, line: 8, column: 29)
!33 = !DILocation(line: 9, column: 9, scope: !32)
!34 = !DILocation(line: 9, column: 16, scope: !32)
!35 = !DILocation(line: 10, column: 5, scope: !32)
!36 = !DILocation(line: 8, column: 25, scope: !27)
!37 = !DILocation(line: 8, column: 5, scope: !27)
!38 = distinct !{!38, !30, !39, !40}
!39 = !DILocation(line: 10, column: 5, scope: !24)
!40 = !{!"llvm.loop.mustprogress"}
!41 = !DILocation(line: 11, column: 7, scope: !16)
!42 = !DILocation(line: 12, column: 5, scope: !16)
!43 = !DILocation(line: 13, column: 16, scope: !16)
!44 = !DILocation(line: 13, column: 18, scope: !16)
!45 = !DILocation(line: 13, column: 12, scope: !16)
!46 = !DILocation(line: 13, column: 5, scope: !16)
!47 = distinct !DISubprogram(name: "foo", scope: !3, file: !3, line: 16, type: !48, scopeLine: 16, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!48 = !DISubroutineType(types: !49)
!49 = !{null, !8}
!50 = !DILocalVariable(name: "i", arg: 1, scope: !47, file: !3, line: 16, type: !8)
!51 = !DILocation(line: 16, column: 14, scope: !47)
!52 = !DILocation(line: 17, column: 9, scope: !53)
!53 = distinct !DILexicalBlock(scope: !47, file: !3, line: 17, column: 9)
!54 = !DILocation(line: 17, column: 11, scope: !53)
!55 = !DILocation(line: 17, column: 9, scope: !47)
!56 = !DILocation(line: 18, column: 9, scope: !57)
!57 = distinct !DILexicalBlock(scope: !53, file: !3, line: 17, column: 16)
!58 = !DILocation(line: 19, column: 5, scope: !57)
!59 = !DILocation(line: 20, column: 13, scope: !60)
!60 = distinct !DILexicalBlock(scope: !53, file: !3, line: 19, column: 12)
!61 = !DILocation(line: 20, column: 9, scope: !60)
!62 = !DILocation(line: 22, column: 1, scope: !47)
!63 = distinct !DISubprogram(name: "baz", scope: !3, file: !3, line: 24, type: !64, scopeLine: 24, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!64 = !DISubroutineType(types: !65)
!65 = !{null}
!66 = !DILocation(line: 25, column: 9, scope: !67)
!67 = distinct !DILexicalBlock(scope: !63, file: !3, line: 25, column: 9)
!68 = !DILocation(line: 25, column: 11, scope: !67)
!69 = !DILocation(line: 25, column: 9, scope: !63)
!70 = !DILocation(line: 26, column: 9, scope: !71)
!71 = distinct !DILexicalBlock(scope: !67, file: !3, line: 25, column: 16)
!72 = !DILocation(line: 27, column: 5, scope: !71)
!73 = !DILocation(line: 28, column: 13, scope: !74)
!74 = distinct !DILexicalBlock(scope: !67, file: !3, line: 27, column: 12)
!75 = !DILocation(line: 28, column: 9, scope: !74)
!76 = !DILocation(line: 30, column: 1, scope: !63)
!77 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 32, type: !78, scopeLine: 32, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!78 = !DISubroutineType(types: !79)
!79 = !{!8, !8, !80}
!80 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !81, size: 64)
!81 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !82, size: 64)
!82 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!83 = !DILocalVariable(name: "argc", arg: 1, scope: !77, file: !3, line: 32, type: !8)
!84 = !DILocation(line: 32, column: 14, scope: !77)
!85 = !DILocalVariable(name: "argv", arg: 2, scope: !77, file: !3, line: 32, type: !80)
!86 = !DILocation(line: 32, column: 27, scope: !77)
!87 = !DILocalVariable(name: "h", scope: !77, file: !3, line: 33, type: !8)
!88 = !DILocation(line: 33, column: 9, scope: !77)
!89 = !DILocation(line: 34, column: 9, scope: !90)
!90 = distinct !DILexicalBlock(scope: !77, file: !3, line: 34, column: 9)
!91 = !DILocation(line: 34, column: 14, scope: !90)
!92 = !DILocation(line: 34, column: 9, scope: !77)
!93 = !DILocation(line: 35, column: 13, scope: !94)
!94 = distinct !DILexicalBlock(scope: !90, file: !3, line: 34, column: 19)
!95 = !DILocation(line: 35, column: 9, scope: !94)
!96 = !DILocation(line: 36, column: 5, scope: !94)
!97 = !DILocation(line: 37, column: 9, scope: !98)
!98 = distinct !DILexicalBlock(scope: !90, file: !3, line: 36, column: 12)
!99 = !DILocation(line: 38, column: 9, scope: !98)
!100 = !DILocation(line: 40, column: 5, scope: !77)
!101 = !DILocation(line: 41, column: 9, scope: !77)
!102 = !DILocation(line: 41, column: 5, scope: !77)
!103 = !DILocation(line: 42, column: 5, scope: !77)
!104 = !DILocation(line: 43, column: 1, scope: !77)
