; ModuleID = 'func_call.bc'
source_filename = "func_call.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@arr = dso_local global [100 x i32] zeroinitializer, align 16, !dbg !0
@l = dso_local global i32 0, align 4, !dbg !6
@.str = private unnamed_addr constant [24 x i8] c"This should be skipped\0A\00", align 1
@.str.1 = private unnamed_addr constant [28 x i8] c"This should be skipped too\0A\00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c"%d\00", align 1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @bar(i32 %0) #0 !dbg !16 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !19, metadata !DIExpression()), !dbg !20
  call void @llvm.dbg.declare(metadata i32* %3, metadata !21, metadata !DIExpression()), !dbg !22
  store i32 0, i32* %3, align 4, !dbg !23
  br label %4, !dbg !25

4:                                                ; preds = %12, %1
  %5 = load i32, i32* %3, align 4, !dbg !26
  %6 = load i32, i32* %2, align 4, !dbg !28
  %7 = icmp slt i32 %5, %6, !dbg !29
  br i1 %7, label %8, label %15, !dbg !30

8:                                                ; preds = %4
  %9 = load i32, i32* %3, align 4, !dbg !31
  %10 = sext i32 %9 to i64, !dbg !33
  %11 = getelementptr inbounds [100 x i32], [100 x i32]* @arr, i64 0, i64 %10, !dbg !33
  store i32 0, i32* %11, align 4, !dbg !34
  br label %12, !dbg !35

12:                                               ; preds = %8
  %13 = load i32, i32* %3, align 4, !dbg !36
  %14 = add nsw i32 %13, 1, !dbg !36
  store i32 %14, i32* %3, align 4, !dbg !36
  br label %4, !dbg !37, !llvm.loop !38

15:                                               ; preds = %4
  %16 = load i32, i32* %2, align 4, !dbg !41
  %17 = sub nsw i32 %16, 1, !dbg !42
  %18 = sext i32 %17 to i64, !dbg !43
  %19 = getelementptr inbounds [100 x i32], [100 x i32]* @arr, i64 0, i64 %18, !dbg !43
  %20 = load i32, i32* %19, align 4, !dbg !43
  ret i32 %20, !dbg !44
}

; Function Attrs: nofree nosync nounwind readnone speculatable willreturn
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @foo(i32 %0) #0 !dbg !45 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !48, metadata !DIExpression()), !dbg !49
  %3 = load i32, i32* %2, align 4, !dbg !50
  %4 = icmp slt i32 %3, 0, !dbg !52
  br i1 %4, label %5, label %6, !dbg !53

5:                                                ; preds = %1
  call void @baz(), !dbg !54
  br label %9, !dbg !56

6:                                                ; preds = %1
  %7 = load i32, i32* %2, align 4, !dbg !57
  %8 = call i32 @bar(i32 %7), !dbg !59
  br label %9

9:                                                ; preds = %6, %5
  ret void, !dbg !60
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @baz() #0 !dbg !61 {
  %1 = load i32, i32* @l, align 4, !dbg !64
  %2 = icmp slt i32 %1, 0, !dbg !66
  br i1 %2, label %3, label %5, !dbg !67

3:                                                ; preds = %0
  %4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i64 0, i64 0)), !dbg !68
  br label %8, !dbg !70

5:                                                ; preds = %0
  %6 = load i32, i32* @l, align 4, !dbg !71
  %7 = call i32 @bar(i32 %6), !dbg !73
  br label %8

8:                                                ; preds = %5, %3
  ret void, !dbg !74
}

declare dso_local i32 @printf(i8*, ...) #2

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main(i32 %0, i8** %1) #0 !dbg !75 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca i32, align 4
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  call void @llvm.dbg.declare(metadata i32* %4, metadata !81, metadata !DIExpression()), !dbg !82
  store i8** %1, i8*** %5, align 8
  call void @llvm.dbg.declare(metadata i8*** %5, metadata !83, metadata !DIExpression()), !dbg !84
  call void @llvm.dbg.declare(metadata i32* %6, metadata !85, metadata !DIExpression()), !dbg !86
  %7 = load i32, i32* %4, align 4, !dbg !87
  %8 = icmp sgt i32 %7, 2, !dbg !89
  br i1 %8, label %9, label %12, !dbg !90

9:                                                ; preds = %2
  %10 = load i32, i32* %4, align 4, !dbg !91
  %11 = call i32 @bar(i32 %10), !dbg !93
  br label %14, !dbg !94

12:                                               ; preds = %2
  %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i64 0, i64 0)), !dbg !95
  store i32 -1, i32* %3, align 4, !dbg !97
  br label %17, !dbg !97

14:                                               ; preds = %9
  %15 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i64 0, i64 0), i32* %6), !dbg !98
  %16 = load i32, i32* %6, align 4, !dbg !99
  call void @foo(i32 %16), !dbg !100
  store i32 0, i32* %3, align 4, !dbg !101
  br label %17, !dbg !101

17:                                               ; preds = %14, %12
  %18 = load i32, i32* %3, align 4, !dbg !102
  ret i32 %18, !dbg !102
}

declare dso_local i32 @__isoc99_scanf(i8*, ...) #2

attributes #0 = { noinline nounwind optnone uwtable "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nofree nosync nounwind readnone speculatable willreturn }
attributes #2 = { "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!12, !13, !14}
!llvm.ident = !{!15}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "arr", scope: !2, file: !3, line: 3, type: !9, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 12.0.1", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5, splitDebugInlining: false, nameTableKind: None)
!3 = !DIFile(filename: "func_call.c", directory: "/home/shank/code/research/llvm-passes-lab/assignment2-skeleton/testcases/function_prioritization")
!4 = !{}
!5 = !{!0, !6}
!6 = !DIGlobalVariableExpression(var: !7, expr: !DIExpression())
!7 = distinct !DIGlobalVariable(name: "l", scope: !2, file: !3, line: 4, type: !8, isLocal: false, isDefinition: true)
!8 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!9 = !DICompositeType(tag: DW_TAG_array_type, baseType: !8, size: 3200, elements: !10)
!10 = !{!11}
!11 = !DISubrange(count: 100)
!12 = !{i32 7, !"Dwarf Version", i32 4}
!13 = !{i32 2, !"Debug Info Version", i32 3}
!14 = !{i32 1, !"wchar_size", i32 4}
!15 = !{!"clang version 12.0.1"}
!16 = distinct !DISubprogram(name: "bar", scope: !3, file: !3, line: 6, type: !17, scopeLine: 6, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!17 = !DISubroutineType(types: !18)
!18 = !{!8, !8}
!19 = !DILocalVariable(name: "i", arg: 1, scope: !16, file: !3, line: 6, type: !8)
!20 = !DILocation(line: 6, column: 13, scope: !16)
!21 = !DILocalVariable(name: "j", scope: !16, file: !3, line: 7, type: !8)
!22 = !DILocation(line: 7, column: 9, scope: !16)
!23 = !DILocation(line: 8, column: 12, scope: !24)
!24 = distinct !DILexicalBlock(scope: !16, file: !3, line: 8, column: 5)
!25 = !DILocation(line: 8, column: 10, scope: !24)
!26 = !DILocation(line: 8, column: 17, scope: !27)
!27 = distinct !DILexicalBlock(scope: !24, file: !3, line: 8, column: 5)
!28 = !DILocation(line: 8, column: 21, scope: !27)
!29 = !DILocation(line: 8, column: 19, scope: !27)
!30 = !DILocation(line: 8, column: 5, scope: !24)
!31 = !DILocation(line: 9, column: 13, scope: !32)
!32 = distinct !DILexicalBlock(scope: !27, file: !3, line: 8, column: 29)
!33 = !DILocation(line: 9, column: 9, scope: !32)
!34 = !DILocation(line: 9, column: 16, scope: !32)
!35 = !DILocation(line: 10, column: 5, scope: !32)
!36 = !DILocation(line: 8, column: 25, scope: !27)
!37 = !DILocation(line: 8, column: 5, scope: !27)
!38 = distinct !{!38, !30, !39, !40}
!39 = !DILocation(line: 10, column: 5, scope: !24)
!40 = !{!"llvm.loop.mustprogress"}
!41 = !DILocation(line: 11, column: 16, scope: !16)
!42 = !DILocation(line: 11, column: 18, scope: !16)
!43 = !DILocation(line: 11, column: 12, scope: !16)
!44 = !DILocation(line: 11, column: 5, scope: !16)
!45 = distinct !DISubprogram(name: "foo", scope: !3, file: !3, line: 14, type: !46, scopeLine: 14, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!46 = !DISubroutineType(types: !47)
!47 = !{null, !8}
!48 = !DILocalVariable(name: "i", arg: 1, scope: !45, file: !3, line: 14, type: !8)
!49 = !DILocation(line: 14, column: 14, scope: !45)
!50 = !DILocation(line: 15, column: 9, scope: !51)
!51 = distinct !DILexicalBlock(scope: !45, file: !3, line: 15, column: 9)
!52 = !DILocation(line: 15, column: 11, scope: !51)
!53 = !DILocation(line: 15, column: 9, scope: !45)
!54 = !DILocation(line: 16, column: 9, scope: !55)
!55 = distinct !DILexicalBlock(scope: !51, file: !3, line: 15, column: 16)
!56 = !DILocation(line: 17, column: 5, scope: !55)
!57 = !DILocation(line: 18, column: 13, scope: !58)
!58 = distinct !DILexicalBlock(scope: !51, file: !3, line: 17, column: 12)
!59 = !DILocation(line: 18, column: 9, scope: !58)
!60 = !DILocation(line: 20, column: 1, scope: !45)
!61 = distinct !DISubprogram(name: "baz", scope: !3, file: !3, line: 22, type: !62, scopeLine: 22, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!62 = !DISubroutineType(types: !63)
!63 = !{null}
!64 = !DILocation(line: 23, column: 9, scope: !65)
!65 = distinct !DILexicalBlock(scope: !61, file: !3, line: 23, column: 9)
!66 = !DILocation(line: 23, column: 11, scope: !65)
!67 = !DILocation(line: 23, column: 9, scope: !61)
!68 = !DILocation(line: 24, column: 9, scope: !69)
!69 = distinct !DILexicalBlock(scope: !65, file: !3, line: 23, column: 16)
!70 = !DILocation(line: 25, column: 5, scope: !69)
!71 = !DILocation(line: 26, column: 13, scope: !72)
!72 = distinct !DILexicalBlock(scope: !65, file: !3, line: 25, column: 12)
!73 = !DILocation(line: 26, column: 9, scope: !72)
!74 = !DILocation(line: 28, column: 1, scope: !61)
!75 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 30, type: !76, scopeLine: 30, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!76 = !DISubroutineType(types: !77)
!77 = !{!8, !8, !78}
!78 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !79, size: 64)
!79 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !80, size: 64)
!80 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!81 = !DILocalVariable(name: "argc", arg: 1, scope: !75, file: !3, line: 30, type: !8)
!82 = !DILocation(line: 30, column: 14, scope: !75)
!83 = !DILocalVariable(name: "argv", arg: 2, scope: !75, file: !3, line: 30, type: !78)
!84 = !DILocation(line: 30, column: 27, scope: !75)
!85 = !DILocalVariable(name: "h", scope: !75, file: !3, line: 31, type: !8)
!86 = !DILocation(line: 31, column: 9, scope: !75)
!87 = !DILocation(line: 32, column: 9, scope: !88)
!88 = distinct !DILexicalBlock(scope: !75, file: !3, line: 32, column: 9)
!89 = !DILocation(line: 32, column: 14, scope: !88)
!90 = !DILocation(line: 32, column: 9, scope: !75)
!91 = !DILocation(line: 33, column: 13, scope: !92)
!92 = distinct !DILexicalBlock(scope: !88, file: !3, line: 32, column: 19)
!93 = !DILocation(line: 33, column: 9, scope: !92)
!94 = !DILocation(line: 34, column: 5, scope: !92)
!95 = !DILocation(line: 35, column: 9, scope: !96)
!96 = distinct !DILexicalBlock(scope: !88, file: !3, line: 34, column: 12)
!97 = !DILocation(line: 36, column: 9, scope: !96)
!98 = !DILocation(line: 38, column: 5, scope: !75)
!99 = !DILocation(line: 39, column: 9, scope: !75)
!100 = !DILocation(line: 39, column: 5, scope: !75)
!101 = !DILocation(line: 40, column: 5, scope: !75)
!102 = !DILocation(line: 41, column: 1, scope: !75)
