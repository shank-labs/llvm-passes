#include <stdio.h>

void log_mem_access(void *addr, int value, int flag) {
    if (flag == 0) {
        printf("Reading value 0x%x from %p\n", value, addr);

    } else if (flag == 1) {
        printf("Writing value 0x%x to %p\n", value, addr);
    }
}

int main() {
    int x = 1;
    /* log_mem_access(&x, x, 0); */

    int *y = &x;
    /* log_mem_access(&y, (long)&x, 0); */

    int **z = &y;
    /* log_mem_access(&z, (long)&y, 0); */

    /* int y = 2; */
    /* log_mem_access(&x, y, 1); */

    return 0;
}
