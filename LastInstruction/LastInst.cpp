#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/Pass.h>
#include <llvm/Support/Debug.h>

using namespace llvm;

namespace {
    struct LastInstPass : public FunctionPass {
        static char ID;
        LastInstPass() : FunctionPass(ID) {}

        bool runOnFunction(Function &F) {
            dbgs() << "[+] Visiting function: " << F.getName() << '\n';

            Instruction *lastInst = nullptr;

            for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I) {
                lastInst = &*I;
            }

            if (lastInst)
                dbgs() << "    [+] last instruction: " << *lastInst << '\n';
            else
                dbgs() << "    [+] last istruction is nullptr" << '\n';

            return false;
        }
    };

    char LastInstPass::ID = 0;

    static RegisterPass<LastInstPass> X("lastInst", "Last Instruction of function", false, false);
} // namespace
