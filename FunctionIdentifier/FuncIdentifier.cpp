#include <llvm/IR/Function.h>
#include <llvm/Pass.h>
#include <llvm/Support/Debug.h>

using namespace llvm;

namespace {
    struct FuncIdentPass : public FunctionPass {
        static char ID;

        FuncIdentPass() : FunctionPass(ID) {}

        bool runOnFunction(Function &F) override {
            dbgs() << F.getName() << '\n';
            return false;
        }
    };
} // namespace

char FuncIdentPass::ID = 0;

static RegisterPass<FuncIdentPass> X("funcIdent", "Function Identifier Pass", false, false);
