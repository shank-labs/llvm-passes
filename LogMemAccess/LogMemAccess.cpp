#include "llvm/IR/InstrTypes.h"
#include <llvm/IR/Constants.h>
#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/Casting.h>
#include <llvm/Support/Debug.h>

using namespace llvm;

namespace {
    struct LogMemAccessPass : public ModulePass {
        static char ID;

        LogMemAccessPass() : ModulePass(ID) {}
        ~LogMemAccessPass() {}

        Function *getLogMemAccessFn(Module *m) {
            // log_mem_access function
            Function *f = m->getFunction("log_mem_access");
            if (!f) {
                dbgs() << "log_mem_access() needs to be included in the module.\n";
                exit(1);
            }
            return f;
        }

        FunctionType *logMemAccessFnType(LLVMContext &C) {
            Type *returnType = Type::getVoidTy(C);
            std::vector<Type *> args{Type::getInt8PtrTy(C), Type::getInt32Ty(C),
                                     Type::getInt32Ty(C)};
            return FunctionType::get(returnType, args, false);
        }

        void instrumentLoad(LoadInst *li) {
            // - log mem access after load instruction
            dbgs() << "load: " << li << '\n';

            Function *f = getLogMemAccessFn(li->getModule());

            // LoadInst:
            // %4 = load i32, i32* %2, align 4

            // Create call to log_mem_access:
            // %2 = bitcast i32* %2 to i8*
            // %4 = load i32, i32* %2, align 4
            // call void @log_mem_access(i8* %3, i32 %4, i32 0)
            // TODO:
        }

        void instrumentStore(StoreInst *si) {
            // - log mem access before load instruction

            dbgs() << "*************************************\n";

            IRBuilder<> builder(&(*si->getIterator()));

            // store i32 0, i32* %1, align 4
            dbgs() << "store: ";
            si->dump();

            // Create call to log_mem_access:
            // %2 = bitcast i32* %2 to i8*
            // %4 = load i32, i32* %2, align 4
            // call void @log_mem_access(i8* %3, i32 %4, i32 0)
            // TODO:

            // prepare fn args
            Value *addr = si->getOperand(1);
            dbgs() << "addr: ";
            addr->dump();
            addr = builder.CreatePointerCast(addr, IntegerType::getInt8PtrTy(si->getContext()));
            dbgs() << "addr: ";
            addr->dump();

            Value *val = si->getOperand(0);
            dbgs() << "val: ";
            val->dump();
            if (val->getType()->isPointerTy()) {
                val = builder.CreatePtrToInt(val, IntegerType::getInt32Ty(si->getContext()));
                dbgs() << "val: ";
                val->dump();
            }

            Constant *flag = ConstantInt::get(Type::getInt32Ty(si->getContext()), 1);
            dbgs() << "flag: ";
            flag->dump();

            std::vector<Value *> args{addr, val, flag};

            Function *logMemAccessFn = getLogMemAccessFn(si->getModule());
            dbgs() << "logMemAccessFn type: ";
            logMemAccessFn->getFunctionType()->dump();

            builder.CreateCall(logMemAccessFn, args);
        }

        bool handleInstruction(llvm::Instruction &i) {

            if (LoadInst *li = dyn_cast<LoadInst>(&i)) {
                instrumentLoad(li);
                return true;

            } else if (StoreInst *si = dyn_cast<StoreInst>(&i)) {
                instrumentStore(si);
                return true;
            };

            return false;
        }

        bool runOnModule(Module &m) override {
            bool edited = false;

            for (auto &currFunc : m) {
                dbgs() << "[+]" << currFunc.getName() << '\n';

                // skip log_mem_access function
                if (currFunc.getName() == "log_mem_access") {
                    dbgs() << "skipping...\n";
                    continue;
                }

                for (auto &currBB : currFunc) {
                    for (auto &currInst : currBB) {
                        edited |= handleInstruction(currInst);
                    }
                }
            }

            return edited;
        }
    };

    char LogMemAccessPass::ID = 0;

    static RegisterPass<LogMemAccessPass>
        X("logMemAccess", "Pass to instrument all memory accesses (load/store)", false, false);

} // namespace
